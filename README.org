#+TITLE: UI-DESIGN
#+AUTHOR: VLEAD
#+DATE: [2018-05-16 Wed]
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil'

* UI DESIGN FOR LINKED LIST EXPERIMENT
- Here are the UI artefacts for our experiment on Linked List and the images we used  on our VLEAD project

* Repository Structure 
  #+BEGIN_EXAMPLE
    ui-design/
    |--- README.org
    |--- init.sh
    |--- makefile
    |--- src/
         |--- index.org
         |--- img/
         |--- ui-artefacts/
  #+END_EXAMPLE
